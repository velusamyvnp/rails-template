json.extract! qrcode, :id, :code, :desc, :created_at, :updated_at
json.url qrcode_url(qrcode, format: :json)
