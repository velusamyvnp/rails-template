class CreateQrcodes < ActiveRecord::Migration[5.1]
  def change
    create_table :qrcodes do |t|
      t.string :code
      t.string :desc

      t.timestamps
    end
  end
end
